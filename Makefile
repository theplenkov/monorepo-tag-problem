sync.script:
	npm set-script sync 'git diff v$${npm_package_version} --stat --relative --quiet || npm version $${npm_new_version}' --workspaces

postversion.script:
	npm set-script postversion '' --workspaces




