
#The goal

This sample repository is an example of NodeJS monorepo where each package may have an own tagged pipeline
To achieve that we tag the corresponding commit with a relevant message. So it supposed that when we call `npm version patch` command - it should generate us 7 tags: 1 main and 6 for subpackages. When we push with follow-tags it is supposed to have 7 tag pipelines.

#The problem
Unfortunately it doesn't happen. There is no explanation yet why. I see only 3-4 pipelines starting max. It looks like things go wrongly somewhere.

This is how it looks:
![image.png](./image.png)



